const db = require('./db');
const helper = require('../helper');
const config = require('../config/config');

async function getMultiple(page = 1) {
  const offset = helper.getOffset(page, config.listPerPage);
  const rows = await db.query(
    'SELECT id, "user" FROM "user" OFFSET $1 LIMIT $2', 
    [offset, config.listPerPage]
  );
  const data = helper.emptyOrRows(rows);
  const meta = {page};

  return {
    data,
    meta
  }
}


async function create(user) {
  console.log("Usuario : ", user);
  const result = await db.query(
    'INSERT INTO "user" ("user") VALUES ($1) RETURNING *',
    [user.user]
  );
  let message = 'Error in creating user';

  if (result.length) {
    message = 'User created successfully';
  }

  return {message};
}

module.exports = {
  getMultiple,
  create
}
