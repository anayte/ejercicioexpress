// Declaracion e instancias 
var express = require('express') 
var app = express()
bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
var userRouter = require('./routes/users');
var port = process.env.PORT || 3000  

// Visualización 
app.get('/', function(req, res) {
  res.json({ mensaje: 'Probando ando inicial' })   
})

app.get('/index', function(req, res) {
  res.json({ mensaje: 'Probando ando index' })  
})

app.post('/', function(req, res) {
  res.json({ mensaje: 'ando probando inicial' })   
})

app.del('/', function(req, res) {
  res.json({ mensaje: ' andaba probando ' })  
})

app.use('/users', userRouter);

// Inciar servicio 
app.listen(port)
console.log(' Esta vivop ' + port)
