const express = require('express');
const router = express.Router();
const users = require('../services/users');

// Lista de usuarios
router.get('/', async function(req, res, next) {
  try {
    res.json(await users.getMultiple(req.query.page));
  } catch (err) {
    console.error(`No se encontró el usuario `, err.message);
    next(err);
  }
});

// Creación de usuarios 
router.post('/', async function(req, res, next) {
 //console.log("esto trae req: ", req); 
 console.log("Usuario : ", req.body); 
 try {
    res.json(await users.create(req.body));
  } catch (err) {
    next(err);
    console.error(`No se agregó al usuario `, err.message);
  }
});

module.exports = router;
